def l_system(mot,dico,k):
    mot_sortie = mot
    for i in range(k):
        m =""
        for lettre in mot_sortie:
            if lettre in dico:
                m += dico[lettre]
            else : 
                m += lettre
        mot_sortie = m
    return mot_sortie

# Test de la fonction
algue = "A"
regles = {'A':'AB ','B':'A ',' ':' '}
generation = 6
resultat = l_system(algue,regles,generation)
print(resultat)
assert resultat == "AB A AB AB A AB A AB AB A AB AB A ", "erreur !"