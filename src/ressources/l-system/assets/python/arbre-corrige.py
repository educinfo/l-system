import turtle

def l_system(mot,dico,generation):
    # Copie du mot initial
    mot_courant = mot
    for _ in range(generation):
        mot_suivant = ""
        for lettre in mot_courant:
            if lettre in dico:
                mot_suivant += dico[lettre]
            else :
                mot_suivant += lettre
        mot_courant = mot_suivant
    return mot_courant

def trace(tortue,motif):
    taille = 40
    angle = 20
    stylo = 3
    tortue.penup()
    tortue.goto(0,0)
    tortue.setheading(90)
    tortue.pendown()
    positions = []
    for lettre in motif :
        if lettre == 'B':
            tortue.pensize(stylo)
            tortue.pencolor('brown')
            tortue.forward(taille)
        elif lettre == 'F':
            tortue.pensize(stylo)
            tortue.pencolor('green')
            tortue.forward(taille)
        elif lettre == 'g':
            tortue.left(angle)
        elif lettre == 'd':
            tortue.right(angle)
        elif lettre == '(':
            positions.append((tortue.position(),tortue.heading()))
            taille /= 1.2
            angle /=1.2
            stylo /= 1.4
        elif lettre ==')':
            p,h = positions.pop()
            tortue.penup()
            tortue.setposition(p[0],p[1])
            tortue.setheading(h)
            tortue.pendown()
            taille *= 1.2
            angle *= 1.2
            stylo *= 1.4

###
# Programme principal
###

motif = "F"
transformations = {'F':'B(gF)(dF)'}

assert l_system(motif,transformations,2)=="B(gB(gF)(dF))(dB(gF)(dF))" ,"Erreur à la génération 2"
assert l_system(motif,transformations,3)== "B(gB(gB(gF)(dF))(dB(gF)(dF)))(dB(gB(gF)(dF))(dB(gF)(dF)))","Erreur à la génération 3"

# Dessin avec 4 générations
motif = l_system(motif,transformations,4)
tortue = turtle.Turtle()
trace(tortue,motif)
turtle.done()