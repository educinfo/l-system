## Tracer un arbre

### De la feuille à l'arbre

Les l-system ont été conçus pour simuler la croissance des végétaux.
Prenons l'exemple suivant, simplement constitué de feuilles et de branches :

<img class="img-fluid" src="ressources/l-system/assets/img/arbre.png">

Le motif de base est une feuille que nous allons noter F.

Il y a un autre symbôle que nous utiliserons, la branche, notée B.

On voit que chaque branche sera prolongée par deux feuilles. Pour pouvoir tracer cet arbre,
nous aurons besoin de deux choses :

- pouvoir aller à gauche ou à droite. On prendra les symbôles g et d
- mémoriser la position pour pouvoir y revenir. On utilisera les parenthèses : La parenthèse ouvrante indique une mémorisation,
  la parenthèse fermante la restitution.

L'ensemble des symbôles sera donc : { F, B, g, d, (, ) }

L'axiome de départ est F

Pour construire l'arbre, chaque feuille deviendra une branche (elle se solidifie) et fait croître deux feuilles. Il y a
une unique règle de dérivation :

{ F &rarr; B(gF)(dF) }

:::todo

Donner la valeur du L-system à la deuxième et à la troisième génération

:::

:::correction
L'axiome de départ est F

La première génération est B(gF)(dF)

La seconde génération est B(gB(gF)(dF))(dB(gF)(dF))

La troisième génération est B(gB(gB(gF)(dF))(dB(gF)(dF)))(dB(gB(gF)(dF))(dB(gF)(dF)))
:::

### Le tracé de l'arbre

Nous allons maintenant passer au tracé de l'arbre. Chacun des symbôles va être traduit par une opération sur la tortue :

- F va faire une ligne droite de couleur verte
- B va faire une ligne droite de couleur marron
- d va faire tourner à droite
- g va faire tourner à gauche
- ( va faire mémoriser la position
- ) va faire restaurer la dernière position mémorisée.

:::todo
Vous allez maintenant pouvoir coder le programme qui permet d'afficher les arbres. Vous aurez deux étapes, gérées par deux fonctions :

- la création du motif du l-system pour une génération donnée (choisir 4 ou 5)
- le tracé à partir du motif

Vous compléterez le fichier proposé.

<div class="essais">
    <button type="button" data-has-blockly="false" data-has-graphics="true"
        data-has-shell="false"
        data-active-tab="consigne"
        data-consigne-url="ressources/l-system/assets/consignes/arbre.html"
        data-python-id="koch"
        data-python-url="ressources/l-system/assets/python/arbre.py"
        class="btn btn-success btn-python">
        Compléter le code
    </button>
</div>
:::
