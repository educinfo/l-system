def remplacer_algue(mot):
    mot_suivant = ""
    for lettre in mot :
        if lettre == "A":
            mot_suivant += "AB "
        if lettre == "B":
            mot_suivant +="A "
    return mot_suivant

# Test de la fonction
algue = "A"
print(algue)
for k in range(6):
    algue = remplacer_algue(algue)
    print(algue)