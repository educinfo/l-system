<?php
/*

	Pages des tutoriel d'apprentissage de p5js
*/
global $pages;

// Bases du javascript
$menu_activite = array(
	"titre" => "L-System",
	"contenu" => [
		'intro',
        'grammaire',
        'turtle',
        'dessin',
		'arbre'
	]
);

// pages des bases du javascript
$l_system_pages = array(
	'intro' => array(
		"template" => 'l-system/introduction.twig.html',
		"menu" => 'l-system',
		'page_precedente' => NULL,
		'page_suivante' => 'grammaire',
		'titre' => 'Introduction',
		"css" => 'ressources/l-system/assets/css/l-system.css'
	),
    'grammaire' => array(
		"template" => 'l-system/grammaire.twig.html',
		"menu" => 'l-system',
		'page_precedente' => 'intro',
		'page_suivante' => 'turtle',
		'titre' => 'Grammaire',
		"css" => 'ressources/l-system/assets/css/l-system.css'
	),
    'turtle' => array(
		"template" => 'l-system/turtle.twig.html',
		"menu" => 'l-system',
		'page_precedente' => 'grammaire',
		'page_suivante' => 'dessin',
		'titre' => 'Turtle',
		"css" => 'ressources/l-system/assets/css/l-system.css'
	),
    'dessin' => array(
		"template" => 'l-system/dessin.twig.html',
		"menu" => 'l-system',
		'page_precedente' => 'turtle',
		'page_suivante' => 'arbre',
		'titre' => 'Représentation',
		"css" => 'ressources/l-system/assets/css/l-system.css'
	),
    'arbre' => array(
		"template" => 'l-system/arbre.twig.html',
		"menu" => 'l-system',
		'page_precedente' => 'dessin',
		'page_suivante' => Null,
		'titre' => 'Un arbre',
		"css" => 'ressources/l-system/assets/css/l-system.css'
	)
);

$pages = array_merge($pages, $l_system_pages);
