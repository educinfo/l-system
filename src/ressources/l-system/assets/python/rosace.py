import turtle

turtle.speed(0)
 
for i in range(80):
    turtle.circle(5*i)
    turtle.circle(-5*i)
    turtle.left(i)

turtle.done()