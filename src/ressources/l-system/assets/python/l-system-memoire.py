import turtle

def l_system(mot,dico,k):
    """ entrée : mot est le mot initial
    dico est l'ensemble des règles de réécriture
    k est le nombre de générations
    sortie : le mot après k générations
    """
    mot_sortie = mot
    # Mettez votre code ici
    for i in range(k):
        m=""
        for lettre in mot_sortie:
            if lettre in dico:
                m += dico[lettre]
            else : 
                m += lettre
        mot_sortie = m
        print(mot_sortie)
    return mot_sortie

def trace(t,mot,longueur):
    """
    entree : - t : l'objet tortue que l'on utilise
             - mot : le mot qui va servir au tracé
             - longueur : la longueur de chaque avancée
    """
    memoire = []
    l = longueur
    # Mettez le code ici
    for lettre in mot :
        if lettre == 'A':
            t.forward(l)
        elif lettre == 'g':
            t.left(30)
        elif lettre == 'd':
            t.right(30)
        elif lettre =="m":
            memoire.append((l,t.position(),t.heading()))
            l *= 0.7
        elif lettre == 'r':
            l,p,h = memoire.pop()
            t.goto(p)
            t.setheading(h)


# Définition du système
mot = "A"
longueur = 60
regles = {'A':'AmgArmdAr'}
generation = 3

# Tracé
tortue = turtle.Turtle()
tortue.shape('classic')
tortue.goto(0,0)
tortue.setheading(90)
expression = l_system(mot,regles,generation)
trace(tortue,expression,longueur)
turtle.done()