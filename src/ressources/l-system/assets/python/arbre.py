import turtle

def l_system(mot,dico,generation):
    # Copie du mot initial
    mot_courant = mot
    # Mettez ici le code pour transformer k fois le mot_courant

    return mot_courant

def trace(tortue,motif):
    taille = 40 # Taille des segments
    angle = 20 # angle à gauche et droite
    stylo = 3 # épaisseur du trait
    tortue.penup()
    tortue.goto(0,0)
    tortue.setheading(90)
    tortue.pendown()
    # Mettez ici le code pour tracer l'arbre.


###
# Programme principal
###

motif = "F"
transformations = {'F':'B(gF)(dF)'}

assert l_system(motif,transformations,2)=="B(gB(gF)(dF))(dB(gF)(dF))" ,"Erreur à la génération 2"
assert l_system(motif,transformations,3)== "B(gB(gB(gF)(dF))(dB(gF)(dF)))(dB(gB(gF)(dF))(dB(gF)(dF)))","Erreur à la génération 3"

# Dessin avec 4 générations
motif = l_system(motif,transformations,5)
tortue = turtle.Turtle()
trace(tortue,motif)
turtle.done()