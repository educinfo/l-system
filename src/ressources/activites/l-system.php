<?php

register_activity('l-system',array(
		'category'=>'NSI1',
		'section'=>'NSI1algo',
		'type'=>'url',
		'titre'=>'Les L-System',
		'auteur'=>"Laurent COOPER",
		'URL'=>'index.php?page=intro&activite=l-system',
		'commentaire'=>"Les L-system sont des systèmes dynamiques permettant des constructions itératives",
		'directory'=>'l-system',
		'icon'=>'fas fa-tree-palm',
		'prerequis'=>NULL
	)
);