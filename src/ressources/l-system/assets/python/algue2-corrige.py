def remplacer_dico(mot,dico):
    mot_suivant = ""
    for lettre in mot :
        if lettre in dico:
            mot_suivant += dico[lettre]
    return mot_suivant

# Test de la fonction
algue = "A"
print(algue)
regles = {'A':'AB ','B':'A ',' ':' '}
for k in range(6):
    algue = remplacer_dico(algue,regles)
    print(algue)