def l_system(mot,dico,k):
    """ entrée : mot est le mot initial
    dico est l'ensemble des règles de réécriture
    k est le nombre de générations
    sortie : le mot après k générations
    """
    mot_sortie = mot
    # Mettez votre code
    return mot_sortie

# Test de la fonction
initial = "A"
regles = {'A':'AgAddAgA'}
generation = 2
resultat = l_system(initial,regles,generation)
print(resultat)
assert resultat == "AgAddAgAgAgAddAgAddAgAddAgAgAgAddAgA", "erreur !"