import turtle

def l_system(mot,dico,k):
    """ entrée : mot est le mot initial
    dico est l'ensemble des règles de réécriture
    k est le nombre de générations
    sortie : le mot après k générations
    """
    mot_sortie = mot
    # Mettez votre code ici
    return mot_sortie

def trace(t,mot,longueur):
    """
    entree : - t : l'objet tortue que l'on utilise
             - mot : le mot qui va servir au tracé
             - longueur : la longueur de chaque avancée
    """
    # Mettez le code ici
    pass

# Définition du système
mot = "A"
longueur = 9
regles = {'A':'AgAddAgA'}
generation = 3

# Tracé
tortue = turtle.Turtle()
turtle.goto(0,0)
expression = l_system(mot,regles,generation)
trace(tortue,expression,longueur)
turtle.done()